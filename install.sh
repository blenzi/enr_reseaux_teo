# From https://github.com/MTES-MCT/didoscalie
git clone https://github.com/MTES-MCT/didoscalie
cd didoscalie
R CMD build .
R CMD check didoscalie_0.1.0.9000.tar.gz --no-manual
R CMD INSTALL didoscalie_0.1.0.9000.tar.gz

R -e """
install.packages('devtools')
install.packages('remotes')
install.packages('archive')
install.packages('rpostgis')
install.packages('filesstrings')
install.packages('xlsx')
install.packages('ggthemes')
install.packages('ggiraph')
install.packages('kableExtra')
install.packages('plotly')
install.packages('shinydashboard')
install.packages('shinyBS')
install.packages('shinycssloaders')
install.packages('stringdist')
install.packages('bsplus')
install.packages('geojson')
install.packages('leaflet.extras')

devtools::install_github('MTES-MCT/didoscalie')

devtools::install_github('MaelTheuliere/COGiter')

devtools::install_github('pachevalier/tricky')

devtools::install_github('mtes-mct/didor')

devtools::install_github('antuki/COGugaison')

remotes::install_gitlab('dreal-datalab/datalibaba')

# Avoid bug with CRAN version: https://github.com/r-spatial/mapview/issues/420
remotes::install_github('r-spatial/mapview')

"""
