FROM rocker/shiny-verse:4.2.0

RUN apt-get update -y && \
    apt-get install libpq-dev -y && \
    apt-get install libudunits2-dev libjq-dev libproj-dev libgdal-dev -y

COPY DESCRIPTION .
RUN install2.r remotes
RUN Rscript -e "remotes::install_deps()"
COPY app app

ARG SHINY_PORT=3838
EXPOSE $SHINY_PORT
RUN echo "local({options(shiny.port = ${SHINY_PORT}, shiny.host = '0.0.0.0')})" >> /usr/local/lib/R/etc/Rprofile.site

CMD ["R", "-e", "shiny::runApp('app')"]
