# Chargement des packages : 9 sec, mapview est parmis les plus couteux
library(lubridate)
library(sf)
library(sp)
library(rgdal)
library(readxl)

library(shinydashboard)
library(shiny)
library(shinyBS)
library(shinycssloaders)
library(bsplus)
library(htmltools)
library(htmlwidgets)
library(markdown)
library(patchwork)
library(png)

library(leaflet)
library(leaflet.extras)
library(mapview)
library(leafpop)
# library(leafem)
library(geojsonsf)
library(geojson)

library(ggiraph)
library(ggthemes)
library(cowplot)

library(kableExtra)
library(DT)
library(aws.s3)
library(shinyscreenshot)
library(tidyverse)

#if(!webshot::is_phantomjs_installed()) {webshot::install_phantomjs(force = FALSE)}

options(scipen=999)
# options("encoding"="UTF-8")

# chargement des données, moins d'un centieme de seconde
# setwd("app")

#récupération des données internes depuis le disque ou S3
if (file.exists("data_netoyee.RData")) {
  load("data_netoyee.RData")
} else {
  s3load("data_netoyee.RData", bucket="projet-connaissance-enr", region="")
}
